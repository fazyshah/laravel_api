<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        if ($request->has('role_id')&&$request['role_id']!='') {
            $user = new User($request->all());
            $user->role_id=$request['role_id'];
            if (!$user->save()) {
                throw new HttpException(500);
            }

            if (!Config::get('boilerplate.sign_up.release_token')) {
                return response()->json([
                    'status' => 'ok',
                    'user' => $user
                ], 201);
            }

            $token = $JWTAuth->fromUser($user);
            return response()->json([
                'status' => 'ok',
                'user' => $user,
                'token' => $token
            ], 201);
        }
        else{
            return response()->json(['success'=>false,'message'=>'Role ID is not present'],406);
}
    }


}
