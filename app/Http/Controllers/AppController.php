<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;

class AppController extends Controller
{
    //
    public function addUser(Request $request){
        $user=User::create([
            'name'=>$request['name'],
            'email'=>$request['email'],
            'password'=>bcrypt($request['password']),
            'role_id'=>$request['role_id']
        ]);
        return response()->json(['success'=>true,'user'=>$user],200);
    }
}
